#################################################################################################################################################################
Question 2. How can you establish a connection between two or many different Jenkins jobs (hosted on distinct servers) to pass the parameters / upstream - downstream jobs etc ..( without using Master-Slave connection in a secured way)
##################################################################################################################################################################

You can trigger Jobs hosted on another jenkins master  in different ways
1. cURL based invocation of jobs 

 curl -v -X POST jenkins_url/job/job_name/buildWithParameters --data token=your_token --user jenkins_user_email:jenkins_user_password --data-urlencode json= '{"parameter1":"value1", "parameter2":"value2"}'


2. You can use remote job trigger in the jobs configuration of the job which would require to generate the  security token for user based authentication.



#####################################################################################################################################
Question 3. Write a script OR Convey a proven mechanism to retrieve the URL of artifacts uploaded in Nexus / Artifactory via the Jenkins job.
######################################################################################################################################
You can retrieve the artifacts GAV cor-ordinates and release version 
and use below wget to retrieve artifacts from Nexus 3 

wget --no-check-certificate "${repo}/repository/snapshots/${groupIdUrl}/${artifactId}/${version}/${artifactId}-${versionTimestamped}${classifier}.${type}" 


####################################################################################################################################################
Question 4. Prepare a Jenkins pipeline job (DSL / groovy script) to satisfy following CI-Cd pipeline :
•	Send approval email to Release Manager after the application packaging phase 
•	Trigger the Jenkins job on reception of the approval email for the above job .
#####################################################################################################################################################
For this I have consider Poll Mailbox plugin
https://github.com/jenkinsci/poll-mailbox-trigger-plugin
Which polls a particular mail box for and email with particular text and proceed ahead with build or timesout for which I
have used timeout wrapper


node {
 stage ('Build TPR')
 {
  try {
    def buildStatus =  build job: 'XYZ', parameters: [string(name: 'pqr', value: 'cba')]
 	def buildResult = buildStatus.getResult()
	if (buildResult == 'SUCCESS'){
		notifySuccessful()
		stage('Build DEF'){
			timeout(time: 30, unit: 'MINUTES') {
                build job: 'DEF', parameters: [string(name: 'pqr', value: 'cba')]
			}
		}
	}
  }catch (e) {
    notifyFailed()
    throw e
  	}
 }
}


def notifySucsessful {
mail from: "", 
   to: "123@abc.com",
   subject: "Jenkins",
   mimeType: "text/html",
   body: "SUCESS"
}

def notifyFailed {
mail from: "", 
   to: "123@abc.com",
   subject: "Jenkins",
   mimeType: "text/html",
   body: "FAILED"
}